import { render, screen } from '@testing-library/react';
import GameList from './GameList';

test('renders learn react link', () => {
  render(<GameList />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
