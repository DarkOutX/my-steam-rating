import './GameList.scss';
import GameElement from './../GameElement/GameElement'
import { useState, useRef, useEffect } from 'react';

function GameList(props) {
  const container = useRef(null);
  const textContainer = useRef(null);
  
  const rawCode = (data) => {
    let output = `${data.date} | `;
    for(let i = 1; i <= 10; i++) {
      if(data.rating === 10) {
        output += "ːstarfullː";
      } else if(i > data.rating) {
        output += "ːstaremptyː";
      } else {
        output += "ːcsgostarː";
      }
    }
    output += ` | ${data.name}\n`;
    return output;
  }

  let textRaw = props.games.map(game => (
    rawCode(game) 
  )).join("");

  const [textLength, setTextLength] = useState(0);

  useEffect(() => {
    container.current.style.width = container.current.offsetWidth + "px";
    
    setTextLength(toUTF8Array(textRaw).length);
  },[])

  const copyText = (e) => {
    if(props.isRaw) {
      let elem = textContainer.current;
      elem.removeAttribute("disabled");
      elem.select();
      document.execCommand('copy');
      elem.disabled = true;
    }
  }

  const list = () => {
    if(props.isRaw) {
      return (<textarea 
          className="games__list" 
          ref={textContainer} 
          defaultValue={textRaw}
          disabled
        />)
    } else {
      return (<div className="games__list">
        { props.games.map(game => (
          <GameElement key={game.appId} {...game} />
        ))}
      </div>)
    }
  }

  const symbolCounter = (amount) => {
    if(props.isRaw) {
      let classMod = "";
      if(amount > 8000) { classMod = "error" } 
      else if (amount > 6500) { classMod = "warning" };

      return (<span className={`games__symbol-counter ${classMod}`}>
        {amount}/8000
      </span>)
    }
  }

  return (
    <div className={`games ${(props.isRaw)?"games__fixed-h":""}`} ref={container} onClick={copyText}>
      <h1>DarkOutX Story Rating: {symbolCounter(textLength)}</h1>
      {list()}
    </div>
  );
}

function toUTF8Array(str) {
    const utf8 = [];
    
	for (let i=0; i < str.length; i++) {
        let charcode = str.charCodeAt(i);
        
		if (charcode < 0x80) {
			utf8.push(charcode)
		}
		else if (charcode < 0x800) {
            utf8.push(
				0xc0 | (charcode >> 6), 
				0x80 | (charcode & 0x3f),
			);
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(
				0xe0 | (charcode >> 12), 
				0x80 | ((charcode>>6) & 0x3f), 
                0x80 | (charcode & 0x3f),
			);
        }
        // surrogate pair
        else {
            i++;
            charcode = ((charcode&0x3ff)<<10) | (str.charCodeAt(i)&0x3ff);

            utf8.push(
				0xf0 | (charcode >>18), 
                0x80 | ((charcode>>12) & 0x3f), 
                0x80 | ((charcode>>6) & 0x3f), 
                0x80 | (charcode & 0x3f),
			);
        }
    }
	
    return utf8;
}

export default GameList;
