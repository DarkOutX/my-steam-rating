import { render, screen } from '@testing-library/react';
import Preloader from './Preloader';

test('renders learn react link', () => {
  render(<Preloader />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
