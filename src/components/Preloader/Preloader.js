import './Preloader.scss';
import preloader from "./../../assets/imgs/preloader.svg";

function Preloader() {
  return (
    <div className="preloader">
      <img src={preloader} alt="preloader" />
    </div>
  );
}

export default Preloader;
