import './GameAdderForm.scss';
import GameRatingSlider from './../GameRatingSlider/GameRatingSlider';
import { useState, useRef, useEffect } from 'react';

function GameAdderForm({isVisible, onSumbit, onClose}) {
  const [rating, setRating] = useState(1);
  const inputDate = useRef(null);

  useEffect(() => {
    inputDate.current.valueAsDate = new Date();
  }, [])

  let className = 'game-add-form';
  if (isVisible) {
    className += ' game-add-form__visible';
  }

  const handleForm = (e) => {
    e.preventDefault();

    const formatDate = (date) => {
      //any to dd.mm.yy
      let dateRaw = new Date(date),
          dateFormed = "";
      
      dateFormed += dateRaw.getDay() + 1 + ".";
      dateFormed += dateRaw.getMonth() + 1 + ".";
      dateFormed += dateRaw.getFullYear() - 2000;
      if(dateFormed.length < 8) dateFormed = "0" + dateFormed; //leading zero

      return dateFormed;
    }

    let dataRaw = new FormData(e.target),
        data = {};
      
    for (let [key, value] of dataRaw.entries()) { 
      if(key === "date") value = formatDate(value);
      if(key === "rating") value = Number(value);
      data[key] = value;
    }

    e.target.reset();
    inputDate.current.valueAsDate = new Date();
    onClose();
    onSumbit(data);
  }

  return (
    <div className={className}>
      <div className="game-add-form__blackout" onClick={ onClose }></div>
      <form onSubmit={handleForm} >
          <label>Game:</label>
          <input name="name" required placeholder="Half-Life" />
          <label>Completed:</label>
          <input name="date" type="date" ref={inputDate} required />
          <label>Rating:</label>
          <GameRatingSlider rating={rating} onMouseOver={setRating} />
          <input name="rating" type="hidden" value={rating} required />
          <label>AppID:</label>
          <input name="appId" placeholder="235490" required pattern="[0-9]+"/>
          <input type="submit" required />
      </form>
    </div>
  );
}

export default GameAdderForm;
