import { render, screen } from '@testing-library/react';
import GameAdder from './GameAdder';

test('renders learn react link', () => {
  render(<GameAdder />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
