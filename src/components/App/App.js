import './App.scss';

import Background from '../Background/Background';
import Preloader from '../Preloader/Preloader';
import GameList from '../GameList/GameList';
import GameAdderForm from '../GameAdderForm/GameAdderForm';
import ButtonsPanel from '../ButtonsPanel/ButtonsPanel';

import {useState, useEffect} from 'react';

function App() {

    const [isFormVisible, setFormVisibility] = useState(false);
    const [isRawCode, setIsRawCode] = useState(false);
    const [games, setGames] = useState([]);

    useEffect(() => {
        fetch("./data.json").then(res => res.json()).then(setGames);
    }, []);

    const switchAdderForm = (e) => {
        setFormVisibility(!isFormVisible);
    }

    const switchCode = (e) => {
        setIsRawCode(!isRawCode);
    }

    const addGame = (game) => {
        setGames([game, ...games]);
    }

    if (!games.length) {
        return (
            <div className="main-wrapper">
                <Background />
                <Preloader />
            </div>
        );
    }

    return (
        <div className="main-wrapper">
            <Background/>
            <GameList games={games} isRaw={isRawCode}/>
            <GameAdderForm isVisible={isFormVisible} onSumbit={addGame} onClose={switchAdderForm}/>
            <ButtonsPanel onAddGame={switchAdderForm} onSwitchCode={switchCode}/>
        </div>
    );
}

export default App;
