import { render, screen } from '@testing-library/react';
import GameElement from './GameElement';

test('renders learn react link', () => {
  render(<GameElement />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
