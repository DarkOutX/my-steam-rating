import './GameElement.scss';
import star from './../../assets/imgs/star.png';
import star_no from './../../assets/imgs/star_no.png';
import star_gold from './../../assets/imgs/star_gold.png';


function GameElement(props) {
  const starList: JSX.Element[] = [];
  
  for (let i = 1; i <= 10; i++) {
    if(props.rating === 10) {
      starList.push(<img key={i} src={star_gold} alt="game-star" />);
    } else if(i > props.rating) {
      starList.push(<img key={i} src={star_no} alt="game-star" />);
    } else {
      starList.push(<img key={i} src={star} alt="game-star" />);
    }
  }
  
  return (
    <a
      className="game-element"
      href={`https://steamcommunity.com/id/DarkOutX/recommended/${props.appId}/`} 
      target="_blank"
      rel="noreferrer"
    >
      <span>{props.date}</span>
      <div className="game-element__rating">
        {starList}
      </div>
      <span>{props.name}</span>
    </a>
  );
}

export default GameElement;
