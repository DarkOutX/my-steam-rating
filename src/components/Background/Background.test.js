import { render, screen } from '@testing-library/react';
import Background from './Background';

test('renders learn react link', () => {
  render(<Background />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
