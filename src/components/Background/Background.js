import './Background.scss';
import bgImg from './../../assets/imgs/default_bg.png';

function Background() {
  /* 
  const getProfileBackground = async (username) => {
    if(!username) return;
    var req = new XMLHttpRequest();
    req.open('GET', `http://localhost/steamPage.php?profile=${username}`, true);   
    req.setRequestHeader("Access-Control-Allow-Origin", "*");
    req.send(null);

    let result = await new Promise((resolve, reject) => {
        req.onload = () => {
            if(req.status == 200) {
                let parser = new DOMParser(),
                    xmlDoc = parser.parseFromString(req.responseText,"text/html"),
                    bgVideo = xmlDoc.querySelector(".profile_animated_background video"),
                    bgVideoSource = xmlDoc.querySelector(".profile_animated_background video source"),
                    bgImage = xmlDoc.querySelector(".no_header.profile_page.has_profile_background"),
                    userName = xmlDoc.querySelector(".actual_persona_name").innerText;
        
                console.log(userName);
                if(bgVideo) {
                    resolve( [bgVideoSource.src, true, bgVideo.poster] );
                } else if (bgImage) {
                    resolve( [bgImage.style.backgroundImage.replace(/^url\(\"(.+)\"\)$/gi, "$1"), false] );
                } else {
                    resolve( );
                    // resolve( [mediaData.background.default_src, false] );
                }
            } else {
                reject({ error: req.error })
            }
        }
    })
    
    return result; 
  }
 */
  return (
    <div className="background">
      <img src={bgImg} alt="background"/>
    </div>
  );
}

export default Background;
