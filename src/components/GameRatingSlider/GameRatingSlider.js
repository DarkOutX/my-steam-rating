import './GameRatingSlider.scss';
import star from './../../assets/imgs/star.png';
import star_no from './../../assets/imgs/star_no.png';
import star_gold from './../../assets/imgs/star_gold.png';

function GameRatingSlider(props) {
  const starList: JSX.Element[] = [];

  for(let i = 1; i <= 10; i++) {
    if(props.rating === 10) {
      starList.push(<img key={i} src={star_gold} alt="game-star" value={i} onMouseOver={ ()=>props.onMouseOver(i) } />);
    } else if(i > props.rating) {
      starList.push(<img key={i} src={star_no} value={i} alt="game-star" onMouseOver={ ()=>props.onMouseOver(i) }  />);
    } else {
      starList.push(<img key={i} src={star} value={i} alt="game-star" onMouseOver={ ()=>props.onMouseOver(i) }  />);
    }
  }

  return <div className="rating--slider">
      {starList}
    </div>;
}

export default GameRatingSlider;
