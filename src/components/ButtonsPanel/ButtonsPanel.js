import './ButtonsPanel.scss';
import Button from "./../Button/Button";

import codeImg from "./../../assets/imgs/code.svg";
import addImg from "./../../assets/imgs/add.svg";

function ButtonsPanel({ onSwitchCode, onAddGame }) {
  return (
    <div className="btns-panel">
      <Button className="btns-panel__switch" src={codeImg} alt="Switch code" onClick={ onSwitchCode } />
      <Button className="btns-panel__add btn_small" src={addImg} alt="Add game"  onClick={ onAddGame } />
    </div>
  );
}

export default ButtonsPanel;
