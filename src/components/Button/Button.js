function Button({ className, onClick, src, alt }) {
  return (
    <button 
      className={className}
      onClick={onClick}
    >
      <img src={src} alt={alt} />
    </button>
  );
}

export default Button;
