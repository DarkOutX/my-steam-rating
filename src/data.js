export let games = [{
    "name": "BLACKTAIL",
    "date": "08.03.25",
    "rating": 7,
    "appId": 1532690,
}, {
    "name": "GRIME",
    "date": "20.02.25",
    "rating": 5,
    "appId": 1123050,
}, {
    "name": "The Inn-Sanity",
    "date": "27.01.25",
    "rating": 7,
    "appId": 2688570,
}, {
    "name": "Resident Evil Village",
    "date": "25.01.25",
    "rating": 8,
    "appId": 1196590,
}, {
    "name": "Kujlevka",
    "date": "27.12.24",
    "rating": 6,
    "appId": 1365720,
}, {
    "name": "VLADiK BRUTAL",
    "date": "20.12.24",
    "rating": 6,
    "appId": 1316680,
}, {
    "name": "Lies of P",
    "date": "10.12.24",
    "rating": 10,
    "appId": 1627720,
}, {
    "name": "While We Wait Here",
    "date": "02.11.24",
    "rating": 10,
    "appId": 2213120,
}, {
    "name": "Redeemer",
    "date": "29.10.24",
    "rating": 5,
    "appId": 447290,
}, {
    "name": "Ravenous Devils",
    "date": "26.10.24",
    "rating": 8,
    "appId": 1615290,
}, {
    "name": "Styx: Shards of Darkness",
    "date": "05.09.24",
    "rating": 2,
    "appId": 355790,
}, {
    "name": "Viewfinder",
    "date": "28.07.24",
    "rating": 6,
    "appId": 1382070,
}, {
    "name": "Terminator: Resistance",
    "date": "02.07.24",
    "rating": 7,
    "appId": 954740,
}, {
    "name": "Batman: Arkham City",
    "date": "19.06.24",
    "rating": 7,
    "appId": 200260,
}, {
    "name": "Batman: Arkham Asylum",
    "date": "08.06.24",
    "rating": 6,
    "appId": 35140,
}, {
    "name": "A Plague Tale: Requiem",
    "date": "26.05.24",
    "rating": 10,
    "appId": 1182900
}, {
    "name": "Lorelai",
    "date": "11.05.24",
    "rating": 8,
    "appId": 593960
}, {
    "name": "Downfall",
    "date": "27.04.24",
    "rating": 9,
    "appId": 364390
}, {
    "name": "The Descendant",
    "date": "13.04.24",
    "rating": 9,
    "appId": 351940
}, {
    "name": "What Remains of Edith Finch",
    "date": "09.04.24",
    "rating": 10,
    "appId": 501300
}, {
    "name": "The Invincible",
    "date": "06.01.24",
    "rating": 8,
    "appId": 731040
}, {
    "name": "Alan Wake",
    "date": "02.01.24",
    "rating": 7,
    "appId": 108710
}, {
    "name": "Beyond: Two Souls",
    "date": "12.11.23",
    "rating": 9,
    "appId": 960990
}, {
    "name": "Divinity: Original Sin",
    "date": "05.09.23",
    "rating": 7,
    "appId": 373420
}, {
    "name": "Baldur's Gate 3",
    "date": "25.08.23",
    "rating": 7,
    "appId": 1086940
}, {
    "name": "Detroit: Become Human",
    "date": "31.05.23",
    "rating": 10,
    "appId": 1222140
}, {
    "name": "Heavy Rain",
    "date": "17.01.23",
    "rating": 8,
    "appId": 960910
}, {
    "name": "Before Your Eyes",
    "date": "14.01.23",
    "rating": 10,
    "appId": 1082430
}, {
    "name": "FAR: Lone Sails",
    "date": "08.01.23",
    "rating": 5,
    "appId": 609320
}, {
    "name": "Stray",
    "date": "25.11.22",
    "rating": 8,
    "appId": 1332010
}, {
    "name": "Unforgiving - A Northern Hymn",
    "date": "21.09.22",
    "rating": 5,
    "appId": 747340
}, {
    "name": "Nanotale - Typing Chronicles",
    "date": "19.02.22",
    "rating": 6,
    "appId": 944920
}, {
    "name": "The Walking Dead: The Final Season",
    "date": "08.02.22",
    "rating": 10,
    "appId": 866800
}, {
    "name": "The Walking Dead: A New Frontier",
    "date": "06.02.22",
    "rating": 10,
    "appId": 536220
}, {
    "name": "Transistor",
    "date": "12.12.21",
    "rating": 7,
    "appId": 237930
}, {
    "name": "The Red Strings Club",
    "date": "02.12.21",
    "rating": 3,
    "appId": 589780
}, {
    "name": "Blacksad: Under the Skin",
    "date": "10.02.21",
    "rating": 10,
    "appId": 1003890
}, {
    "name": "Contrast",
    "date": "09.11.20",
    "rating": 8,
    "appId": 224460
}, {
    "name": "Lust for Darkness",
    "date": "07.11.20",
    "rating": 5,
    "appId": 523650
}, {
    "name": "A Plague Tale: Innocence",
    "date": "03.11.20",
    "rating": 10,
    "appId": 752590
}, {
    "name": "Call of Cthulhu",
    "date": "02.11.20",
    "rating": 7,
    "appId": 399810
}, {
    "name": "Batman: The Enemy Within",
    "date": "23.01.20",
    "rating": 8,
    "appId": 675260
}, {
    "name": "Batman - The Telltale Series",
    "date": "29.12.19",
    "rating": 7,
    "appId": 498240
}, {
    "name": "Life is Strange: Before the Storm",
    "date": "16.08.19",
    "rating": 10,
    "appId": 554620
}, {
    "name": "Shadow Warrior 2",
    "date": "23.04.19",
    "rating": 8,
    "appId": 324800
}, {
    "name": "Borderlands 2",
    "date": "13.04.19",
    "rating": 6,
    "appId": 49520
}, {
    "name": "Shadow Warrior",
    "date": "06.04.19",
    "rating": 8,
    "appId": 233130
}, {
    "name": "Firewatch",
    "date": "02.04.19",
    "rating": 10,
    "appId": 383870
}, {
    "name": "Grand Theft Auto V",
    "date": "20.03.19",
    "rating": 10,
    "appId": 271590
}, {
    "name": "Edna & Harvey: Harvey's New Eyes",
    "date": "05.01.19",
    "rating": 8,
    "appId": 219910
}, {
    "name": "Punch Club",
    "date": "11.12.18",
    "rating": 7,
    "appId": 394310
}, {
    "name": "Press X to Not Die",
    "date": "08.12.18",
    "rating": 10,
    "appId": 402330
}, {
    "name": "Epistory - Typing Chronicles",
    "date": "28.06.18",
    "rating": 9,
    "appId": 398850
}, {
    "name": "Red Faction",
    "date": "07.05.18",
    "rating": 6,
    "appId": 20530
}, {
    "name": "Borderlands",
    "date": "27.04.18",
    "rating": 6,
    "appId": 8980
}, {
    "name": "The Last Door (Base + MiniEP)",
    "date": "05.03.18",
    "rating": 7,
    "appId": 284390
}, {
    "name": "Amnesia: A Machine for Pigs",
    "date": "24.02.18",
    "rating": 6,
    "appId": 239200
}, {
    "name": "Root Of Evil: The Tailor",
    "date": "01.02.18",
    "rating": 7,
    "appId": 550310
}, {
    "name": "Oxenfree",
    "date": "29.01.18",
    "rating": 6,
    "appId": 388880
}, {
    "name": "The Walking Dead: Michonne",
    "date": "19.01.18",
    "rating": 8,
    "appId": 429570
}, {
    "name": "Tales from the Borderlands",
    "date": "17.01.18",
    "rating": 10,
    "appId": 330830
}, {
    "name": "Layers of Fear (Base + Inheritance)",
    "date": "16.01.18",
    "rating": 7,
    "appId": 391720
}, {
    "name": "Life is Strange",
    "date": "14.01.18",
    "rating": 8,
    "appId": 319630
}, {
    "name": "SOMA",
    "date": "12.01.18",
    "rating": 8,
    "appId": 282140
}, {
    "name": "Call of Juarez Gunslinger",
    "date": "21.10.17",
    "rating": 9,
    "appId": 204450
}, {
    "name": "Planet Alcatraz 2",
    "date": "26.09.17",
    "rating": 9,
    "appId": 389250
}, {
    "name": "Singularity",
    "date": "19.04.16",
    "rating": 9,
    "appId": 42670
}, {
    "name": "Spec Ops: The Line",
    "date": "25.11.15",
    "rating": 9,
    "appId": 50300
}, {
    "name": "Planet Alcatraz",
    "date": "19.11.15",
    "rating": 9,
    "appId": 289420
}, {
    "name": "Deponia",
    "date": "06.11.15",
    "rating": 8,
    "appId": 292910
}, {
    "name": "The Silent Age",
    "date": "02.11.15",
    "rating": 7,
    "appId": 352520
}, {
    "name": "Dying Light",
    "date": "26.02.15",
    "rating": 8,
    "appId": 239140
}, {
    "name": "To The Moon",
    "date": "12.02.15",
    "rating": 9,
    "appId": 206440
}, {
    "name": "The Cat Lady",
    "date": "10.01.15",
    "rating": 9,
    "appId": 253110
}, {
    "name": "Among The Sleep",
    "date": "07.01.15",
    "rating": 5,
    "appId": 250620
}, {
    "name": "The Walking Dead: Season Two",
    "date": "06.01.15",
    "rating": 10,
    "appId": 261030
}, {
    "name": "The Walking Dead (Base + 400 days)",
    "date": "04.01.15",
    "rating": 10,
    "appId": 207610
}, {
    "name": "The Wolf Among Us",
    "date": "16.12.14",
    "rating": 10,
    "appId": 250320
}, {
    "name": "Mirror's Edge",
    "date": "10.12.14",
    "rating": 8,
    "appId": 17410
}, {
    "name": "Half-Life 2: Episode Two",
    "date": "10.11.14",
    "rating": 10,
    "appId": 420
}, {
    "name": "Brothers - A Tale of Two Songs",
    "date": "05.11.14",
    "rating": 8,
    "appId": 225080
}, {
    "name": "The Darkness II",
    "date": "13.10.14",
    "rating": 9,
    "appId": 67370
}, {
    "name": "Half-Life 2: Episode One",
    "date": "12.09.14",
    "rating": 10,
    "appId": 380
}, {
    "name": "Half-Life 2",
    "date": "10.09.14",
    "rating": 10,
    "appId": 220
}, {
    "name": "Half-Life",
    "date": "31.08.14",
    "rating": 7,
    "appId": 70
}, {
    "name": "Zeno Clash 2",
    "date": "27.08.14",
    "rating": 10,
    "appId": 215690
}, {
    "name": "Portal 2",
    "date": "20.08.14",
    "rating": 10,
    "appId": 620
}, {
    "name": "Zeno Clash",
    "date": "18.08.14",
    "rating": 9,
    "appId": 22200
}, {
    "name": "Portal",
    "date": "07.08.14",
    "rating": 10,
    "appId": 400
}]
